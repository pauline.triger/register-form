/**
 * Réaliser un formulaire d'inscription à un site
 * vérifier les différents input du formulaire
 * input : username, password, confirm-password, tel, mail
 *
 * Vérification à faire sur chaque input.
 * username : vérifier qu'on a bien écrit un username et vérifier que le username contient au moins 8 caractères
 * password : idem que pour le username + vérifier qu'il contient au moins 1 chiffre et au moins un caractère spécial ($^%@)
 * confirm-password : vérifier que le mot de passe est identique à l'input du dessus (password)
 * tel : vérifier qu'il ne contient que des chiffres et qu'il contient 10 caractères de long
 * mail : vérifier qu'il y a bien un mail, et un point après l'@ .
 *
 * Les inputs doivent être de type text, sauf pour password.
 *
 * A la fin du formulaire il doit y avoir un input type submit
 *
 * ne pas mettre de balise form
 *
 * afficher toutes les erreurs de saisie dans une balise contenant une classe error
 *
 * afficher un message de réussite à l'utilisateur "compte créé". La balise contenant ce message doit inclure une classe "success"
 */

$('#submit-info').click(function() {
    var errors = []

    var username = $('#username').val()
    var password = $('#password').val()
    var confirmPassword = $('#confirm-password').val()
    var tel = $('#tel').val()
    var mail = $('#mail').val()

    const p = $('<p></p>')
    const errorMessage = $('<div class="error"></div>')
    const successMessage = $('<div class="success"><p>compte créé</p></div>')

    // Vérification du nom d'utilisateur
    if (username) {
        if (username.length >= 8) {
        } else {
            errors.push(`<p>Identifiant trop court</p>`);
        }
    } else {
        errors.push(`<p>Identifiant obligatoire</p>`);
    }

    // Vérification du mot de passe
    if (password) {
        if (password.length < 8) {
            errors.push(`<p>Mot de passe trop court trop court</p>`)
        }
        if (!(password.match(/\d+/g))) {
            errors.push(`<p>Le mot de passe doit contenir au moins un chiffre</p>`)
        }
        if (!(password.match(/[\$,\^,\%,\@]/g))) {
            errors.push(`<p>Le mot de passe doit contenir au moin un caractère spécial</p>`)
        }
    }

    // vérification que les mots de passe saisis correspondent bien
    if (confirmPassword) {
        if (password !== confirmPassword) {
            errors.push(`<p>Les mots de passe ne correspondent pas</p>`)
        }
    }

    // Vérification du numéro de téléphone
    if (tel) {
        if (tel.length !== 10) {
            errors.push(`<p>Le téléphone est invalide</p>`)
        }

        if (!(/^\d+$/.test(tel))) {
            errors.push(`<p>Le téléphone est invalide</p>`)
        }
    }

    if (mail) {
        if (!(mail.match(/\@.*\./g))) {
            errors.push(`<p>Le mail est invalide</p>`);
        }
    }

    if (errors.length > 0) {
        $('body').append(errorMessage)
        $('.error').append(errors)
    } else {
        $('body').append(successMessage)
    }

})